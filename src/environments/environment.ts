// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebaseConfig: {
    apiKey: "AIzaSyAxjg5IEMQGmT9AwIK6BuqEihYMcrKkZxU",
    authDomain: "test20-d8176.firebaseapp.com",
    databaseURL: "https://test20-d8176.firebaseio.com",
    projectId: "test20-d8176",
    storageBucket: "test20-d8176.appspot.com",
    messagingSenderId: "446105903990",
    appId: "1:446105903990:web:6939e120673d4d007e5057",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
