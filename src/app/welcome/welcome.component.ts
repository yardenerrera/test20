import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Users } from '../interfaces/users';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  email:string;
  

  constructor(private authService:AuthService) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.email = user.email;
    
      }
    ) 
    }
  
    
  

}
