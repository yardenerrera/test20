import { Component, OnInit } from '@angular/core';
import { Main } from '../interfaces/main';
import { MainService } from '../main.service';
import { Comments } from '../interfaces/comments';

@Component({
  selector: 'app-mains',
  templateUrl: './mains.component.html',
  styleUrls: ['./mains.component.css']
})
export class MainsComponent implements OnInit {
  //geting posts from json not from db
  mains$: Main[];
  comments$: Comments[];
 

  constructor(private mainService:MainService) { }
 /* myMassage(){
    this.saved = "Saved for later viewing";
 }*/

  mySave(title:string, body:string){
    this.mainService.addTodos(title,body);
    
 }


  ngOnInit() {
    this.mainService.getComments().subscribe(data => this.comments$ = data)
    this.mainService.getMains().subscribe(data => this.mains$ = data)  
    
  }
  

}
