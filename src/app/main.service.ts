import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore} from '@angular/fire/firestore';
import { Main } from './interfaces/main';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Comments } from './interfaces/comments';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  apiUrl='https://jsonplaceholder.typicode.com/posts/';
  commentUrl='https://jsonplaceholder.typicode.com/posts/1/comments';
  

  constructor(private http: HttpClient, private db: AngularFirestore, private router:Router) { }
  getMains(){
    return this.http.get<Main[]>(this.apiUrl)
  }
  getTodos():Observable<any[]>{
    return this.db.collection('todos').valueChanges({idField:'id'});
     }

     getComments(){
      return this.http.get<Comments[]>(this.commentUrl)
    }
    addLike(){
      const like=0
      this.db.collection('todos').add(like) 

    }

  addTodos(title:String, body:String){
      const todo= {title:title, body:body}
      this.db.collection('todos').add(todo) 
    .then(
        res=>
        { 
        this.router.navigate(['/todos']);
        }
        )
     
    
    }
  deleteTodo(id:string){
    this.db.doc(`todos/${id}`).delete();
  }
  }

