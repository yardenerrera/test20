import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClassifiedService {

  constructor(private db: AngularFirestore,private router:Router) { }
  
  getArticles():Observable<any[]>{
    return this.db.collection('articles').valueChanges({idField:'id'});
     }

  addArticle(category:String, body:String, image:string){
    const article = {category:category, body:body, image:image}
    this.db.collection('articles').add(article)  
    this.router.navigate(['/articles']);
          
  }

  deleteArticle(id:string){
    this.db.doc(`articles/${id}`).delete();
  }

}


