import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos$:Observable<any>;
  todo:String;
  likes = 0;
  userId:string;

  constructor(public mainService:MainService, public authService:AuthService) { }
  
  addLike(){
    this.likes++
  }

  deleteTodo(id){
    this.mainService.deleteTodo(id)
    console.log(id);
  }



  ngOnInit() {
    this.todos$ = this.mainService.getTodos();   
    
  }
 
}
