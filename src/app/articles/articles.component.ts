import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ClassifiedService } from '../classified.service';


@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  articles$:Observable<any[]>;


  deleteArticle(id){
    this.classifiedService.deleteArticle(id)
    console.log(id);
  }

  constructor (private classifiedService:ClassifiedService){}

  ngOnInit() {
    this.articles$ = this.classifiedService.getArticles(); 
  }
 
}
