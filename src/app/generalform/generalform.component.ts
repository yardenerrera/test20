import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralsService } from './../generals.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-generalform',
  templateUrl: './generalform.component.html',
  styleUrls: ['./generalform.component.css']
})
export class GeneralformComponent implements OnInit {

 
  rooms:object[] = [{id:1, name:'colab room 1'},{id:2, name:'colab room 2'},
                     {id:3, name:'colab room 3'}];

  constructor(private generalsService:GeneralsService,
              private authService:AuthService, 
              private router:Router,
              private route: ActivatedRoute) { }

  title:string;
 //author:string;
  room:string;
  id:string;
  userId:string;
  date:Date;
  isEdit:boolean = false;
  buttonText:string = 'Add general' 
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.generalsService.updateGeneral(this.userId, this.id,this.title,this.room, this.date);
    } else {
      console.log('In onSubmit');
      this.generalsService.addGeneral(this.userId,this.title,this.room, this.date)
    }
    this.router.navigate(['/generals']);  
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
        if(this.id) {
          this.isEdit = true;
          this.buttonText = 'Update general'
          this.generalsService.getGeneral(this.userId,this.id).subscribe(
            general => {
            console.log(general.data().room)
            console.log(general.data().title)
            console.log(general.data().date)
            this.room = general.data().room;
            this.date = general.data().date;
            this.title = general.data().title;})        
      }
   })
  }
}
